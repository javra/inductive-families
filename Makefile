all: main.pdf

main.pdf : main.tex references.bib
	latexmk -pdf main.tex

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *.vtc *.ptb *~ main.pdf
