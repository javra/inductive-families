{-# OPTIONS --rewriting --without-K #-}

module IFEx where

open import Lib hiding (id; _∘_)
open import IF
open import IFA
open import IFD
open import IFS

module Constructor {Ωc}(Ω : ConP Ωc) where

  conSᵃ' : ∀{B}(t : TmS Ωc B) → _ᵃS {lsuc lzero} B
  conSᵃ' {U}      t     = TmP Ω (El t)
  conSᵃ' {Π̂S T B} t     = λ τ → conSᵃ' (t $S τ)

  concᵃ' : ∀{Γc}(σ : SubS Ωc Γc) → _ᵃc {lsuc lzero} Γc
  concᵃ' ε       = lift tt
  concᵃ' (σ , t) = concᵃ' σ , conSᵃ' t

  contᵃ' : ∀{Γc}(σ : SubS Ωc Γc){B}(t : TmS Γc B) → (t ᵃt) (concᵃ' σ) ≡ conSᵃ' {B} (t [ σ ]t)
  contᵃ' ε       t             = ⊥-elim (Tm∙c t)
  contᵃ' (σ , s) (var vvz)     = refl
  contᵃ' (σ , s) (var (vvs x)) = contᵃ' σ (var x)
  contᵃ' (σ , s) (t $S τ)      = happly (contᵃ' (σ , s) t) τ

  conPᵃ' : ∀{A}(tP : TmP Ω A) → (A ᵃP) (concᵃ' id)
  conPᵃ' {El a}   tP = coe (contᵃ' id a ⁻¹) tP
  conPᵃ' {a ⇒P A} tP = λ α → conPᵃ' {A} (tP $P coe (contᵃ' id a) α)
  conPᵃ' {Π̂P T A} tP = λ τ → conPᵃ' {A τ} (tP $̂P τ)

  conᵃ' : ∀{Γ}(σP : SubP Ω Γ) → (Γ ᵃC) (concᵃ' id)
  conᵃ' εP        = lift tt
  conᵃ' (σP ,P t) = conᵃ' σP , conPᵃ' t

  contPᵃ' : ∀{Γ}(σP : SubP Ω Γ){A}(tP : TmP Γ A) → (tP ᵃtP) (conᵃ' σP) ≡ conPᵃ' {A} (tP [ σP ]tP)
  contPᵃ' εP         tP              = ⊥-elim (TmP∙ tP)
  contPᵃ' (σP ,P sP) (varP vvzP)     = refl
  contPᵃ' (σP ,P sP) (varP (vvsP v)) = contPᵃ' σP (varP v)
  contPᵃ' (σP ,P sP) (tP $P uP)      = contPᵃ' (σP ,P sP) tP ⊗ contPᵃ' (σP ,P sP) uP
                                       ◾ conPᵃ' & (_$P_ (tP [ σP ,P sP ]tP) & coecoe⁻¹ (contᵃ' id _) _)
  contPᵃ' (σP ,P sP) (tP $̂P τ)       = happly (contPᵃ' _ tP) τ

concᵃ : ∀{Ωc}(Ω : ConP Ωc) → _ᵃc {lsuc lzero} Ωc
concᵃ {Ωc} Ω = Constructor.concᵃ' Ω id

conᵃ : ∀{Ωc}(Ω : ConP Ωc) → (Ω ᵃC) (concᵃ Ω)
conᵃ Ω = Constructor.conᵃ' Ω idP

module Eliminator {Ωc}(Ω : ConP Ωc){ωcᵈ}(ωᵈ : ᵈC {lsuc lzero} Ω ωcᵈ (conᵃ Ω)) where

  open Constructor Ω

  elimSᵃ' : ∀{B}(t : TmS Ωc B) → ˢS B (ᵈt t ωcᵈ)
  elimSᵃ' {U}      a = λ α → coe (ᵈt a _ & (contPᵃ' idP (coe (contᵃ' id a) α)
                                 ◾ coecoe⁻¹' (contᵃ' id a) α))
                                   (ᵈtP {lsuc lzero} {lsuc lzero} (coe (contᵃ' id a) α) ωᵈ)
  elimSᵃ' {Π̂S T B} t = λ τ → elimSᵃ' {B τ} (t $S τ)

  elimcᵃ' : ∀{Γc}(σ : SubS Ωc Γc) → ˢc Γc (ᵈs σ ωcᵈ)
  elimcᵃ' ε       = lift tt
  elimcᵃ' (σ , t) = elimcᵃ' σ , elimSᵃ' t

  elimtᵃ' : ∀{Γc}(σ : SubS Ωc Γc){B}(t : TmS Γc B) → elimSᵃ' (t [ σ ]t) ≡ ˢt t (elimcᵃ' σ)
  elimtᵃ' ε (var ())
  elimtᵃ' (σ , t) (var vvz)     = refl
  elimtᵃ' (σ , t) (var (vvs v)) = elimtᵃ' σ (var v)
  elimtᵃ' σ (t $S τ)            = happly (elimtᵃ' σ t) τ
  
  elimPᵃ' : ∀{A}(tP : TmP Ω A) → ˢP A (elimcᵃ' id) (ᵈtP tP ωᵈ)
  elimPᵃ' {El a}   tP = let ca = contᵃ' id a in
                        let ctP = coe (ca ⁻¹) tP in
                        coe≡' (apd (ˢt a (elimcᵃ' id)) (contPᵃ' idP tP) ◾ happly (elimtᵃ' id a) ctP ⁻¹) ◾
                        tr∘' (ᵈt a ωcᵈ) (contPᵃ' idP tP) (contPᵃ' idP (coe ca ctP) ◾ coecoe⁻¹' ca ctP) ◾
                        l ca (λ tP → (tP ᵃtP) (conᵃ Ω)) (ᵈt a ωcᵈ) tP (λ tP → ᵈtP tP ωᵈ) (contPᵃ' idP)
    where l : {A : Set₁}{B : Set₁}(e : B ≡ A)(g : A → B)(D : B → Set₁)(u : A)(f : (x : A) → D (g x))(h : (x : A) → g x ≡ coe (e ⁻¹) x) → 
            coe (D & ((h (coe e (coe (e ⁻¹) u)) ◾ coecoe⁻¹' e (coe (e ⁻¹) u)) ◾ h u ⁻¹)) (f (coe e (coe (e ⁻¹) u))) ≡ f u
          l refl g D u f h = J (λ _ e → coe (D & (e ◾ e ⁻¹)) (f u) ≡ f u) refl (h u)
  elimPᵃ' {Π̂P T A} tP = λ τ → elimPᵃ' {A τ} (tP $̂P τ)
  elimPᵃ' {a ⇒P A} tP = λ α → let e = contPᵃ' idP (coe (contᵃ' id a) α) ◾ coecoe⁻¹' (contᵃ' id a) α in
                              coe (ˢPAid≡ (ᵃtP≡ e) (ᵈtP≡ e ◾ ᵈtP tP ωᵈ α & happly (elimtᵃ' id a) α)) (elimPᵃ' {A} (tP $P coe (contᵃ' id a) α))
    where ˢPAid≡ : ∀ {α α' αᵈ αᵈ'} (p : α ≡ α') (q : coe (ᵈP A ωcᵈ & p) αᵈ ≡ αᵈ')
                   → ˢP A (elimcᵃ' id) {α} αᵈ ≡ ˢP A (elimcᵃ' id) {α'} αᵈ'
          ˢPAid≡ refl refl = refl
          ᵃtP≡ : ∀ {α α'} (p : α ≡ α') → (tP ᵃtP) (conᵃ Ω) α ≡ (tP ᵃtP) (conᵃ Ω) α'
          ᵃtP≡ refl = refl
          ᵈtP≡ : ∀ {α α' αᵈ} (e : α' ≡ α) → coe (ᵈP A ωcᵈ & ᵃtP≡ e) (ᵈtP tP ωᵈ α' αᵈ) ≡ ᵈtP tP ωᵈ α (coe (ᵈt a ωcᵈ & e) αᵈ)
          ᵈtP≡ refl = refl

  elimᵃ' : ∀{Γ}(σP : SubP Ω Γ) → ˢC Γ (elimcᵃ' id) (ᵈsP σP ωᵈ)
  elimᵃ' εP         = lift tt
  elimᵃ' (σP ,P tP) = elimᵃ' σP , elimPᵃ' tP

elimcᵃ : ∀{Γc}(Γ : ConP Γc){γcᵈ}(γᵈ : ᵈC Γ γcᵈ (conᵃ Γ)) → ˢc Γc γcᵈ
elimcᵃ Γ γᵈ = Eliminator.elimcᵃ' Γ γᵈ id

elimᵃ : ∀{Γc}(Γ : ConP Γc){γcᵈ}(γᵈ : ᵈC Γ γcᵈ (conᵃ Γ)) → ˢC Γ (elimcᵃ Γ γᵈ) γᵈ
elimᵃ Γ γᵈ = Eliminator.elimᵃ' Γ γᵈ idP


--some examples
Γnat : ConP (∙c ▶c U)
Γnat = ∙ ▶P vz ⇒P El vz ▶P El vz

nat : Set₁
nat = ₂ (concᵃ Γnat)

nzero : nat
nzero = ₂ (conᵃ Γnat)

nsucc : nat → nat
nsucc = ₂ (₁ (conᵃ Γnat))

nrec : ∀(P : nat → Set₁)(ps : ∀ n → P n → P (nsucc n))(pz : P nzero) → ∀ n → P n
nrec P ps pz = ₂ (elimcᵃ Γnat (_ , ps , pz))

Γuu : ConP (∙c ▶c U ▶c U)
Γuu = ∙ ▶P El (vs vz) ▶P El vz

uu1 : Set₁
uu1 = ₂ (₁ (concᵃ Γuu))

uu2 : Set₁
uu2 = ₂ (concᵃ Γuu)

st1 : uu1
st1 = ₂ (₁ (conᵃ Γuu))

st2 : uu2
st2 = ₂ (conᵃ Γuu)

postulate N : Set
postulate Nz  : N
postulate Ns  : N → N

Γvec : Set → ConP (∙c ▶c Π̂S N (λ _ → U))
Γvec A = ∙ ▶P El (vz $S Nz) ▶P (Π̂P A (λ a → Π̂P N λ m → (vz $S m) ⇒P El (vz $S Ns m)))

vec : Set → N → Set₁
vec A = ₂ (concᵃ (Γvec A))

vzero : {A : Set} → vec A Nz
vzero = ₂ (₁ (conᵃ (Γvec _)))

vcons : ∀{A : Set}(a : A) n → vec A n → vec A (Ns n)
vcons = ₂ (conᵃ (Γvec _))
