# A Syntax for Mutual Inductive Families

These agda files are the formalized version for the draft of the paper called "A Syntax for Mutual Inductive Families" by Ambrus Kaposi and Jakob von Raumer. 
Most definitions are named by the model they are constituting and the part of the syntax:

| Model                       | Notation | File     |
| --------------------------- | -------- | -------- |
| Standard model              | `ᵃ`      | IFA.agda |
| Nondep. logical relations   | `ᵐ`      | IFM.agda |
| Logical predicates          | `ᵈ`      | IFD.agda |
| Dependent logical relations | `ˢ`      | IFS.agda |

| Syntax element              | Notation in Syntax | Short hand |
| --------------------------- | ------------------ | ---------- |
| Sort type                   | `TyS`              | `S`        |
| Sort context                | `ConS`             | `c`        |
| Sort variable               | `VarS`             |            |
| Sort term                   | `TmS`              | `t`        |
| Sort susbstitution          | `SubS`             | `s`        |
| Point type                  | `TyP`              | `P`        |
| Point context               | `ConP`             | `C`        |
| Point variable              | `VarP`             |            |
| Point term                  | `TmP`              | `tP`       |
| Point substitution          | `SubP`             | `sP`       |

The formalization is comprised of the following files:

## IF.agda

Contains the syntax for indexed inductive types with contexts for sort (`Scon`) and for point constructors (`Con`).

## IFA.agda

Contains the Set interpretation of the syntax described in IF.agda, describing algebras of indexed inductive types.

## IFM.agda

Contains the model for the indexed inductive types which describes morphisms.

## IFD.agda

Contains displayed algebras of indexed inductive types. These algebras depend on an algebra as described in IFA.agda.

## IFS.agda

Contains the indexed inductive type interpretation for the section of the aforementioned displayed algebras.

## IFEx.agda

Shows the existence of inductive families given the internalization of the syntax as given in IF.agda.
